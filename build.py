import codecs
import os
import toml
from buildtools import log, os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.coffeescript import CoffeeBuildTarget
from buildtools.maestro.web import SCSSBuildTarget

AMO_SECRET=None
AMO_ISSUER=None
if os.path.isfile('jwt-credentials.toml'):
  with codecs.open('jwt-credentials.toml','r') as f:
    data=toml.load(f)
    AMO_ISSUER=data['AMO']['issuer']
    AMO_SECRET=data['AMO']['secret']
os_utils.ensureDirExists('dist')
bm = BuildMaestro()
bm.RecognizeType(CoffeeBuildTarget)
bm.RecognizeType(SCSSBuildTarget)
bm.loadRules('Makefile.pmk')
bm.run()
os_utils.copytree('lib', 'dist/lib', verbose=True)
os_utils.copytree('html', 'dist/html', verbose=True)
os_utils.copytree('icons', 'dist/icons', verbose=True)
os_utils.single_copy('manifest.json', 'dist', verbose=True)
with os_utils.Chdir('dist'):
    #os_utils.cmd(['zip','-r', '../torro.xpi', '*'], critical=True, echo=True, show_output=True)
    os_utils.cmd(['web-ext','lint'], critical=True, echo=True, show_output=True)
    if AMO_ISSUER is not None and AMO_SECRET is not None:
      os_utils.cmd(['web-ext','sign', '--api-key='+AMO_ISSUER,'--api-secret='+AMO_SECRET, '-a', '../deploy/','-v'], critical=True, echo=True, show_output=True)
    else:
      os_utils.cmd(['web-ext','build', '-a', '../deploy/','-v'], critical=True, echo=True, show_output=True)
