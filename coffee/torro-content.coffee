
document.addEventListener "click", (e) ->
  if e.target.localName == 'a'
    uri=e.target.getAttribute('href',2)
    if uri.startsWith("magnet:")
      console.log "TORRO: Got magnet #{uri}"
      if DispatchMagnet(uri)
        e.preventDefault()
  return

DispatchMagnet = (uri) ->
  promise = browser.runtime.sendMessage {
    'type': 'magnet',
    'uri': uri
  }
  promise.then null, ->
    console.error "Unable to send magnet to background script."
