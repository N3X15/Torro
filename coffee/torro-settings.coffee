
torro=null
dispatchlist=null
sendReload = () ->
  promise = browser.runtime.sendMessage {
    'type': 'reload'
  }
  promise.then null, ->
    console.error "Unable to send reload to background script."
rebuildAll = () ->
  console.log "Found #{Object.keys(torro.Dispatchers).length} dispatchers."
  dispatchlist.innerHTML=''
  for dispatcher in torro.Dispatchers
    fs = E('fieldset', {'class': 'fsdispatch', 'data-dispatcher-id': dispatcher.ID})
    dispatchlist.appendChild fs
    legend = E('legend',{},document.createTextNode(name))
    fs.appendChild legend

    dispatcher.FillSettings(fs)

    removeButton = E('button', {'class': 'remove', 'data-dispatcher-id':dispatcher.ID}, document.createTextNode('Remove'))
    fs.appendChild removeButton

    dispatcher.SendToForm()
$ ->
  torro = new Torro()
  torro.OnLoaded.addListener ->
    console.log "Loaded, rebuildAll()"
    rebuildAll()
  torro.LoadSettings()
  dispatchlist=document.getElementById('dispatchers')
  rebuildAll()
  $('#cmdAdd').on 'click', (e) ->
    newdis=new DelugeDispatcher()
    newdis.ID=prompt 'Name for dispatcher'
    if newdis.ID == null or newdis.ID == ''
      return
    torro.Dispatchers[newdis.ID]=newdis
    rebuildAll()
    torro.SaveSettings()
    sendReload()
    return
  $('body').on 'change', '#dispatchers', (e) ->
    console.log "Found #{Object.keys(torro.Dispatchers).length} dispatchers."
    for dispatcher in torro.Dispatchers
      dispatcher.GetFromForm()
    torro.SaveSettings()
    sendReload()
    return
  $('body').on 'click', '.remove', (e) ->
    ID = $(this).attr('data-dispatcher-id')
    console.log "Removing dispatcher %s", ID
    newD=[]
    for d in torro.Dispatchers
      if d.ID == ID
        continue
      newD.push d
    torro.Dispatchers=newD
    torro.SaveSettings()
    sendReload()
    rebuildAll()
    return
