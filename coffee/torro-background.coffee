###
TORRO BACKGROUND SCRIPT

Copyright (c) 2016-2017 Rob "N3X15" Nelson <nexisentertainment@gmail.com>

MIT Open-Source License
###
torro = new Torro()
torro.Attach()
torro.LoadSettings()

browser.contextMenus.create({
  id: "torro-link",
  title: "Torro This Link",
  contexts: ["link"]
})

browser.contextMenus.onClicked.addListener (info, tab) ->
  switch info.menuItemId
    when "torro-link"
      storeProm = browser.cookies.getAll {
        'storeId': tab.cookieStoreId
      }
      storeProm.then (cookies) ->
        torro.DispatchDownload info.linkUrl, cookies
        return
  return
