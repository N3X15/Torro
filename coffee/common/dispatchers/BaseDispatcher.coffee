class BaseDispatcher
  constructor: (@Torro, @Type) ->
    @ID = '' # Unique name of dispatcher.
    @Enabled = false

  LoadFromStorage: (data) =>
    @ID=data.ID
    @Enabled=data.Enabled

  SaveToStorage: =>
    return {
      ID: @ID,
      Type: @Type,
      Enabled: @Enabled
    }

  FillSettings: (root) =>
    root.appendChild E("input", {type:"checkbox", id:@prefixID("chkEnabled"), checked:'checked'})

    enabledLabel = E "label", {for : @prefixID("chkEnabled")}
    enabledLabel.appendChild document.createTextNode("Enabled")
    root.appendChild enabledLabel

    wrapTable = E "table", {class: 'subform'}
    root.appendChild wrapTable
    return wrapTable

  prefixID: (x) =>
    return @ID.replace(' ','_') + '_' + x

  SendToForm: () =>
    return

  GetFromForm: () =>
    return

  GetE: (id) =>
    return document.getElementById(@prefixID(id))

  AddInputRow: (table, label, id, inputEl) =>
    cRow = E("tr")
    table.appendChild cRow

    cCell = E("td",{class: 'label', for: @prefixID(id)}, document.createTextNode(label))
    cRow.appendChild cCell

    cCell = E("td",{class: 'input'},inputEl)
    cRow.appendChild cCell

  AddTextboxRow: (table, id, label) =>
    @AddInputRow table, label, id, E("input", {type:'textbox', id: @prefixID(id)})

  AddPasswordRow: (table, id, label) =>
    @AddInputRow table, label, id, E("input", {type:'password', id: @prefixID(id)})

  AddCheckboxRow: (table, id, label) =>
    @AddInputRow table, label, id, E("input", {type:'checkbox', id: @prefixID(id)})

  DispatchMagnet: (url) =>
    return

  DispatchDownload: (download, cookies) =>
    return
