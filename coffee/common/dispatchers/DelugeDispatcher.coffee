###
Sends API requests to Deluge WebAPI.

MIT License
###
class DelugeDispatcher extends BaseDispatcher
  constructor: (torro) ->
    super torro, 'DelugeDispatcher'
    @WebScheme='http'
    @WebPassword='deluge'
    @Address='127.0.0.1'
    @Port=8112
    @Session=null
    @AddPaused=false

  FillSettings: (root) =>
    table = super root
    @AddTextboxRow table, 'scheme', 'WebGUI Scheme:'
    @AddTextboxRow table, 'address', 'WebGUI Address:'
    @AddTextboxRow table, 'port', 'WebGUI Port:'
    @AddPasswordRow table, 'password', 'WebGUI Password:'
    @AddCheckboxRow table, 'paused', "Add Paused"

  SendToForm: =>
    @GetE('scheme').value = @WebScheme
    @GetE('address').value = @Address
    @GetE('port').value = @Port
    @GetE('password').value = @WebPassword
    @GetE('paused').checked = @AddPaused

  GetFromForm: =>
    @WebScheme = @GetE('scheme').value.trim()
    @Address = @GetE('address').value.trim()
    @Port = parseInt(@GetE('port').value.trim())
    @WebPassword = @GetE('password').value.trim()
    @AddPaused = @GetE('paused').checked
    @Session=null

  LoadFromStorage: (data) =>
    super data
    @WebScheme = data.scheme
    @Address = data.address
    @Port = data.port
    @WebPassword = data.password
    @AddPaused = data.paused || false

  SaveToStorage: =>
    data = super()
    data.scheme = @WebScheme
    data.address = @Address
    data.port = @Port
    data.password = @WebPassword
    data.paused = @AddPaused
    return data

  CheckLogin: (loginSuccess) =>
    if @Session==null
      @Session = new DelugeSession "#{@WebScheme}://#{@Address}:#{@Port}", @WebPassword
      @Session.Attach()
    if @Session.sessid == null
      @Session.login @WebPassword, loginSuccess, ->
        browser.notifications.create @ID+"-notify", {
          "type": "basic",
          "iconUrl": browser.extension.getURL("icons/torro-48.png"),
          "title": "Torro",
          "message": "Incorrect login information for #{@ID}."
        }
    else
      loginSuccess()

  GetDelugeOpts: () =>
    return {
      add_paused: @AddPaused
    }

  DispatchMagnet: (url) =>
    @CheckLogin =>
      console.log "TORRO: Dispatching magnet %s to Deluge instance %s", uri, @ID
      @Session.add_torrent_magnet url, @GetDelugeOpts(), (response) ->
        console.log "TORRO: Done!"
        console.log response

  DispatchDownload: (download, cookies) =>
    @CheckLogin =>
      console.log "TORRO: Dispatching download %s to Deluge instance %s", download, @ID
      @Session.download_torrents_from_url download, cookies, (response) =>
        console.log "TORRO: Downloaded to %s.  Now adding...", response.data
        @Session.add_torrents [{path: response.data, options:@GetDelugeOpts()}], =>
          console.log "TORRO: Done!"
          browser.notifications.create @ID+"-notify", {
            "type": "basic",
            "iconUrl": browser.extension.getURL("icons/torro-48.png"),
            "title": "Torro",
            "message": "Torrent successfully added to #{@ID}."
          }
