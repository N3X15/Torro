E = (tagName, attrs, child=null) ->
  e = document.createElement tagName
  for own k,v of attrs
    e.setAttribute k,v
  if child != null and child != undefined
    if Array.isArray(child)
      for item in child
        e.appendChild item
    else
      e.appendChild child
  return e

class TorroObserver
  constructor: () ->
    @listeners=[]

  addListener: (l) =>
    @listeners.push l

  invoke: (args...) =>
    for listener in @listeners
      listener(args...)

class Torro
  constructor: () ->
    @Dispatchers=null
    @OnLoaded = new TorroObserver() # void, Called when LoadSettings finishes.
    @TmpDispatchStruct=[]
    @ShowAlerts=true
    @Hooks={}

  Attach: =>
    @Hooks={
      'OnDownloadCreated': browser.downloads.onCreated.addListener(@OnDownloadCreated)
    }
    browser.runtime.onMessage.addListener @OnMessage

  LoadSettings: () =>
    console.log "TORRO: Loading settings..."
    saveP=browser.storage.local.get null
    @Dispatchers=[]
    @ShowAlerts=false
    on_success = (data) =>
      if data == null or data == undefined
        console.error "Something went wrong, browser storage data is %s", data
        return
      @ShowAlerts=data.show_alerts or true
      for ext in data.dispatchers or []
        newExt = null
        if ext.Type == 'DelugeDispatcher'
          newExt = new DelugeDispatcher(@)
        if newExt == null
          console.warn "Unknown ext.Type=%s!", ext.Type
          continue
        newExt.LoadFromStorage(ext)
        @Dispatchers.push newExt
      @OnLoaded.invoke()
      return
    on_fail = =>
      console.error "Failed to load stored data."
      return
    saveP.then on_success, on_fail

  SaveSettings: =>
    console.log "TORRO: Saving settings..."
    newDispatcherStruct = []
    for dispatcher in @Dispatchers
      newDispatcherStruct.push dispatcher.SaveToStorage()
    console.log "TORRO: Serialized #{newDispatcherStruct.length} dispatchers."
    prom = browser.storage.local.set({
      'show_alerts': @ShowAlerts
      'dispatchers': newDispatcherStruct
    })
    prom.then null, ->
      console.error "Save failed."

  GetSetting: (id, toVar, defaultVal) =>
    getter=(data) =>
      @[toVar] = defaultVal
      if id in data and data[id] != null
        @[toVar] = data[id]
    errhandler=(err) =>
      console.log "Failed to load setting #{id}: #{error}"
    getitem = browser.storage.local.get(id)
    getitem.then(getter,errhandler)
    return

  OnDownloadCreated: (download) =>
    console.log "TORRO: Got DL #{download.url}"
    cancel=false
    if download.mime == "application/x-bittorrent"
      cancel = @DispatchDownload(download, document.cookie)
    if cancel
      browser.downloads.cancel(download.id)
    return

  OnClick: (e) =>
    uri=e.target.getAttribute('href',2)
    if uri.startsWith("magnet:")
      console.log "TORRO: Got magnet #{uri}"
      if @DispatchMagnet(uri)
        e.preventDefault()
    return

  DispatchDownload: (download, cookies) =>
    found=false
    for dispatcher in @Dispatchers
      dispatcher.DispatchDownload(download, cookies)
      found=true
    return found

  DispatchMagnet: (uri) =>
    found=false
    for dispatcher in @Dispatchers
      dispatcher.DispatchMagnet(uri)
      found=true
    return found

  OnMessage: (data) =>
    switch data.type
      when 'magnet'
        @DispatchMagnet data.uri
      when 'download'
        @DispatchDownload data.download
      when 'reload'
        @LoadSettings()
