###
Implementation of DelugeRPC.
###

RPC_RESPONSE = 1
RPC_ERROR = 2
RPC_EVENT = 3

class DelugeRPCResponse
  constructor: (@Request, responseData) ->
    # GOOD: {"id": 0, "result": true, "error": null}
    # BAD:  {"id": 1, "result": null, "error": {"message": "Unknown method", "code": 2}}
    @error=null
    @ID=-1
    @data=null
    @responseData=responseData
    if responseData!=undefined
      #responseData=JSON.parse(responseData)
      @ID=responseData['id']
      @data=responseData['result']
      @error=responseData['error']

  is_error: =>
    @error != null

  get_error_message: =>
    if @error != null
      return @error['message']
    return null

  get_error_id: =>
    if @error != null
      return @error['core']
    return null



class DelugeRPCRequest
  constructor: (@DelugeSession, @Method, @Args=[], @KWArgs={}) ->
    @ID=@DelugeSession.get_req_id()

  send: (onSuccess, onFail) =>
    headers={
      "Content-Type": "application/json;charset=UTF-8"
    }
    #if @DelugeSession.sessid
    #  headers['X-Deluge-Session'] = @DelugeSession.sessid
    fetch @DelugeSession._JsonPath, {
      headers: headers
      method: 'POST'
      credentials: 'include'
      body: JSON.stringify({
        id: @ID
        method: @Method
        params: @Args
        kwargs: @KWArgs
      })
    }
    .then (response) =>
      #console.log response
      return response.json()
    .then (response) =>
      #console.log response
      @handleRPCResponse response, onSuccess, onFail

  handleRPCResponse: (data, onSuccess, onFail) =>
    dresponse = new DelugeRPCResponse(@, data)
    if dresponse.is_error()
      if onFail
        onFail dresponse
        return
      console.error data
      console.error "Failed: #%d - %s", dresponse.get_error_id(), dresponse.get_error_message()
    else
      if onSuccess
        onSuccess dresponse
        return


class DelugeSession
  constructor: (@BaseURI, @Password) ->
    @ReqID=0
    @_JsonPath=@BaseURI+'/json?TORROSESS' # HACK: Differentiate from a "Normal" session in the WebGUI.
    @_UploadPath=@BaseURI+'/upload' # Not used, here for reference.
    @sessid=null

  Attach: =>

  ###
    # HACK: Firefox WebExtensions don't permit cookie reading/writing due to security constraints.
    browser.webRequest.onBeforeSendHeaders.addListener(
      @setCookies,
      {'urls': [@_JsonPath]},
      ["blocking", "requestHeaders"]
    )
    browser.webRequest.onHeadersReceived.addListener(
      @getCookies,
      {'urls': [@_JsonPath]},
      ["responseHeaders"]
    );
  ###
  getCookies: (e) =>
    console.log "TORRO: GOT HEADERS"
    for header of e.responseHeaders
      #console.log header.name
      if header.name.toLowerCase() == "cookie"
        @sessid = getCookie(header.value,'_session_id')
        console.log "TORRO: Session ID found: %s", @sessid
    return {responseHeaders: e.responseHeaders}

  setCookies: (e) =>
    if @sessid != null
      e.requestHeaders.push {
        name: 'Set-Cookie'
        value: '_session_id='+@sessid
      }
    return {requestHeaders: e.requestHeaders}

  get_req_id: =>
    @ReqID++

  login: (password, success, fail) =>
    console.log "TORRO: Logging in..."
    @_request 'auth.login', [password]
    .send (response) =>
      if response and response.is_error()
        throw new Error("Failed to log in.")
      else
        #@sessid = response.data.session_token
        @sessid = getCookie(document.cookies, "_session_id")
        console.log "Login successful. Session ID: %s", @sessid
        success(response)
    , (response) ->
      if fail
        fail(response)


  cookieArrayForDeluge: (cookies) ->
    return (cookie.name+"="+cookie.value for cookie in cookies).join(';')

  download_torrents_from_url: (url, cookies, success) =>
    #console.log cookies
    cookiestr=@cookieArrayForDeluge(cookies)
    req = @_request 'web.download_torrent_from_url', [url, cookiestr]
    req.send (response) ->
      if response and response.is_error()
        throw new Error("Download failed.")
      else
        success(response)

  add_torrents: (torrents, success) =>
    req = @_request 'web.add_torrents', [torrents]
    req.send (response) ->
      if response and response.is_error()
        throw new Error("Download failed.")
      else
        success(response)

  add_torrent_magnet: (magnet, options, success) =>
    req = @_request 'core.add_torrent_magnet', [magnet, options]
    req.send (response) ->
      if response and response.is_error()
        throw new Error("Download failed.")
      else
        success(response)

  _request: (method, args=[], kwargs={}) =>
    new DelugeRPCRequest @, method, args, kwargs
