getCookies = (c=document.cookie) ->
  v = 0
  cookies = {}
  if c.match(/^\s*\$Version=(?:"1"|1);\s*(.*)/)
    c = RegExp.$1
    v = 1
  if v == 0
    c.split(/[,;]/).map (cookie) ->
      parts = cookie.split(RegExp('='), 2)
      name = decodeURIComponent(parts[0].trimLeft())
      value = if parts.length > 1 then decodeURIComponent(parts[1].trimRight()) else null
      cookies[name] = value
      return
  else
    c.match(/(?:^|\s+)([!#$%&'*+\-.0-9A-Z^`a-z|~]+)=([!#$%&'*+\-.0-9A-Z^`a-z|~]*|"(?:[\x20-\x7E\x80\xFF]|\\[\x00-\x7F])*")(?=\s*[,;]|$)/g).map ($0, $1) ->
      name = $0
      value = if $1.charAt(0) == '"' then $1.substr(1, -1).replace(/\\(.)/g, '$1') else $1
      cookies[name] = value
      return
  cookies

getCookie = (source,name) ->
  getCookies(source)[name]
