# Torro Torrent Dispatcher

Torro is an extension for intercepting torrent and magnet links and dispatching them to a given HTTP API.  Currently only supports Deluge.

## Status

Torro is currently under active development and is targeted at Firefox.

+--------+------------+
| Deluge | Supported! |
+--------+------------+
